function renderSites() {
  var banner = new Vue({el: '#banner', data: {content: config.user}});
  var contentArea =
      new Vue({el: '#siteGroups', data: {navData: config.navData}});
}

function checkConfigAndProcess() {
  if (window.config && config.user && config.navData)
    renderSites();
  else {
    if (criticalMessage == '')
      criticalMessage =
          '引用的配置"' + scriptPath + '"不正确！请检查URL与配置文件内容。';
  }
  var message =
      new Vue({el: '#critical-message', data: {content: criticalMessage}});
}

function getQueryString(name) {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
}

var criticalMessage = '';
var fallBackScriptPath = 'js/sampleData.js';
var scriptPath = fallBackScriptPath;

var configSourceType = getQueryString('t');
var configID = getQueryString('id')

var useSampleData = (!configID);

if (useSampleData && (configSourceType != 'sp'))
  criticalMessage = 'URL中设置的导航配置类型为' + configSourceType +
      '，但没有提供对应的ID.正在使用后备导航数据。';

switch (configSourceType) {
  case 'sp':
    scriptPath = fallBackScriptPath;
    break;
  case 'pb':
    scriptPath = 'https://pastebin.com/raw/' + configID;
    break;
  default:
    scriptPath = fallBackScriptPath;
}

var script = document.createElement('script');
script.type = 'text/javascript';
script.src = scriptPath;
script.onload = checkConfigAndProcess;
script.onerror = checkConfigAndProcess;
document.body.appendChild(script);